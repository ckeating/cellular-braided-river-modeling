%% Cellular automata model of a braided stream with vegetation
% 5 pixel version with periodic boundary conditions


function braided_veg_ck_Thomas (nrows, ncolumns, niterations, Cs,... 
filename, write_videos, include_veg)

    

    %% TODO List
    
    % Essentials
    
    % Steps to add Thomas paper functionality (in new file):
        % 1. increase from 3 to 5 cells
        % 2. perform additional calculations for routing potential to each
        % cell
        % 3. calculate net discharge in x and y direction
        
    % Nice-to-haves:
    
    % optional input parameters, see: 
    % https://www.mathworks.com/matlabcentral/answers/164496-how-to-create-an-optional-input-parameter-with-special-name
    
    % Column-wise operation:
    %     switch row/column so that computations of lateral sediment transfer
    %     are done in accordance with the way that matlab reads matrix variables -
    %     column-wise.  Visually need to make the streamflow left to right instead
    %     of up to down, so that lateral sediment transfer happens in the columns
    %     and instead of the rows (the way it currently operates)
    %------------------------------------------------------

    
    %% INPUT VARIABLES

    nrows % Number of rows -- Original: 300
    ncolumns %Number of columns -- Original: 50
    niterations %Number of iterations -- Original: 1000
    nframes = niterations/10 %Number of frames to save for a movie. 
    %   Make sure the number of iterations is divisible evenly by this.
    ncellsdischarge = 30; %Number of cells at the top row receiving discharge -- Original: 50
    q0 = 2000; %Discharge in each cell receiving discharge -- Original: 2000
    s = 100000; %Bed slope -- Original: 100000
    % q0(round(ncolumns/2-ncellsdischarge/2)+1:round(ncolumns/2-
    %   ncellsdischarge/2)+ncellsdischarge) = 25000;
    % q0 = 10000*[0 0 ones(1,ncolumns-4) 0 0]; 
    %   ^ Initial amount of water in top row cells
    N = 0.5; % Exponent in water transfer rules
    m = 2.5; % Exponent in sediment transfer rule
    K = 1E-21; % Coefficient in sediment transfer rule
    %Cs = 1000000; % Sediment concentration that's going to get carried on. -- Original: 300000
                  %  Allows sediment to be carried uphill
    Kl0 = 1E-7; % Coefficient in lateral sediment transfer rule, 
    %   (Murray and Paola 1997, see p. 1006 for more detail)
    Th = double(1e9); % s*q0/2;

    %------------------------------------------------------
    %% CALCULATE STUFF FROM INPUT VARIABLES

    modarg = niterations/nframes; %Argument for modulus

    %------------------------------------------------------
    %% INITIALIZE VARIABLES
    
    % discharge and topo movies:
    Q_movie = zeros(nrows, ncolumns, nframes); 
    % Q_movie stores Q data to make a movie (in separate script)
    % For performance, just save Q values in Q_movie and don't save entire
    % workspace as done previously; make the movie from these Q matrices in
    % another script or at the end of this script

    Topo_movie = zeros(nrows, ncolumns, nframes);
    % Topo_movie stores z data to make movie out of changes in topography 
    %   over time 

    Q = zeros(nrows, ncolumns); % discharge
%     Qi = zeros(1, 3);
    Qi = zeros(1, 5);
    trendz = zeros(nrows, ncolumns);
    plants = uint32(zeros(nrows, ncolumns)); % stores binary values where 1 
    %   indicates presence of a plant and 0 indicates dead(nonexistent) plant
    T_growth = uint32(zeros(nrows, ncolumns)); % stores time (# iterations) 
    %   that each plant has been alive
    T_resurrect = uint32(zeros(nrows, ncolumns)); % stores time (# iterations) 
    %   that a plant has been resurrecting (10 iterations before it becomes 
    %   living again)
    Qlostlat = Q; Qgainedlat = Q; Qlostlong = Q; Qgainedlong = Q; 
    % ^for mass balance and debugging

    %------------------------------------------------------
    %% COME UP WITH STARTING TOPOGRAPHY

    zmax = s*ncolumns*2; %Elevation of the top
    for ii = 1:nrows
        trendz(ii, :) = zmax-s*ii; %Put in the general bed slope trend
    end
    z = trendz + sign(randn(nrows, ncolumns)).*200000.*rand(nrows, ncolumns); 
    % ^ Put in some white noise on the order of the height difference between rows. 
    %NEEDED: M+P used initial topography perturbations with amplitude of 200,000. 
    %Use that instead of s/2?

    z(:,1) = trendz(:,1)+z(:, ncolumns)-trendz(:, ncolumns); 
    % ^ makes bottom of domain same as top corrected for elevation differences

    %------------------------------------------------------
    %% PLOT STARTING TOPOGRAPHY
    % in 2D
    figure(1)
    starttopo = flipud(fliplr(z-trendz));
    subplot(1, 3, 1)
    pcolor(starttopo(1:nrows-1, 2:ncolumns-1))
    colormap gray
    colorbar
    caxis([-2e5 2e5])
    shading flat
    axis equal
    axis image
    title('Starting topography')


    % in 3D
    figure(2)
    subplot(1, 2, 1)
    surfl(z-trendz)
    title('Starting Topography')
    shading interp
    zlim([-1*zmax, zmax])
    %pause
    %------------------------------------------------------
    %% PROCESS

    % [trash, I] = sort(z(2,:)); %Find the lowest cells in the second row and 
    %   allocate discharge to them
    % Q(1,:) = 0;
    % Q(1,I(1:ncellsdischarge)) = q0;
    Q(1,:)= q0; % Adds initial discharge to entire top row
    twoday_dep = zeros(nrows, ncolumns, 100); %Initialize array for calculating 
    %   deposition that has occurred over 2 days 
    %   1/15/18 Colin: how does time correspond to iterations? 


    for n = uint32(1:niterations)
        % print occasional iteration, roughly 1 print per second
        if mod(n,15000/(ncolumns*nrows))==0
            n
        end

        % Initialize net sediment transfer array
        netsed = zeros(nrows, ncolumns); 
        
        % For mass balance and debugging
        Qlostlat = zeros(nrows,ncolumns); 
        
%         D = max(max(max(max(z(1:nrows-1,2:ncolumns)-z(2:nrows,1:ncolumns-1))),...
%             max(max(z(1:nrows-1,1:ncolumns-1)-z(2:nrows,2:ncolumns))))/sqrt(2), ...
%             max(max(z(1:nrows-1,:)-z(2:nrows,:)))); %maximum possible dropoff from row to row
        %     K = min(Kdef, D/2*(min(min((Q.*(D+Cs)-Th).^-m)))); 
                %Set the sediment transport coefficient so that stability is achieved. 
                %This is like dynamic timestepping.
        %     z = [z(:,ncolumns-1) z(:,2:ncolumns-1) z(:,2)]; % Wraps boundary
        %     K = Kdef; % Use this instead of stability condition K from now on
        
        % rows loop:
        for r = uint16(1:nrows-1)
            
            Qnew = zeros(1, ncolumns); %Reset Qnew
            Qsinew = zeros(1, ncolumns); %Reset Qsinew
            Qslnew = zeros(1, ncolumns); %Reset Qslnew
            Qlost = zeros(1, ncolumns); %Reset Qlost
            
            % columns loop
            for c = uint8(1:ncolumns) % added uint8 - max 255 columns
                %% CALCULATE SLOPES
                % Since each cell in the top row receives discharge, make sure
                % slopes and subsequent calculations wrap around like with
                % elevation.  
                
                % Edge cases are c==1,c==2 c==ncolumns-1, c==ncolumns
                if c == 1
                    slps = (z(r,1)-z(r+1,[ncolumns-1 ncolumns 1 2 3]))./[sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                    latslp = z(r,[ncolumns-1 ncolumns 2 3])-z(r,c);
                    latslp = max(0,latslp); 
                elseif c == 2
                    slps = (z(r,ncolumns)-z(r+1,[ncolumns 1 2 3 4]))./[sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                    latslp = z(r,[ncolumns 1 3 4])-z(r,c);
                    latslp = max(0,latslp); 
                elseif c == ncolumns-1
                    slps = (z(r,ncolumns)-z(r+1,[ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 1]))./[sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                    latslp = z(r,[ncolumns-3 ncolumns-2 ncolumns 1])-z(r,c);
                    latslp = max(0,latslp);
                elseif c == ncolumns
                    slps = (z(r,ncolumns)-z(r+1,[ncolumns-2 ncolumns-1 ncolumns 1 2]))./[sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                    latslp = z(r,[ncolumns-2 ncolumns-1 1 2])-z(r,c);
                    latslp = max(0,latslp);  
                else
                    slps = (z(r,c)-z(r+1, c-2:cc+2))./[sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)]; %Slopes for square cells
                    latslp = z(r, [c-2 c-1 c+1 c+2])-z(r,c); %Lateral slopes
                    latslp = max(0, latslp); %Get rid of negative lateral slopes
                end
                %-------------------------------------------------------------
                
                %% CALCULATE DISCHARGE using Thomas et al.

                %% TRANSFER WATER
                if max(slps)>0 %If there is at least 1 positive (downward) slope
                    slps2 = max(0, slps); %Reset negative slopes to 0. 
                    %   NEEDED: Don't reset this variable, since actual slope 
                    %       determines sediment transfers below!!
                    Qi = Q(r,c).*(slps2.^N)/sum(slps2.^N);
                    % ^ Thomas paper, Eq 1.  Qi= discharge received by each
                    % of the downstream cells from the upstream cell
                elseif max(slps)<0 %If there are only negative slopes
                    slps2 = abs(slps); %Take the absolute value of the slopes
                    Qi = Q(r,c)./(slps2.^N.*sum(1./slps2.^N)); 
                    % ^ Q RULE 1b, Murray and Paola 1997
                  
                else % There is a combination of negatives and zeros
                    myzeros = find(slps >= 0);
                    mynegs = find(slps < 0);
                    Qi(myzeros) = Q(r,c)./length(myzeros); % distribute 
                    %   discharge from upstream cell evenly among the downstream cells with slope=0
                    Qi(mynegs) = 0; %#ok<FNDSB>
                end
                
                if abs(sum(Qi) - Q(r,c))>0.00000001, pause, end
                if min(Qi)<0, pause, end
                %--------------------------------------------------------------
                %% TRANSFER SEDIMENT.

                Th0 = 1.218e8; %s*max(q0)/2; %Threshold stream power needed
                %   for sediment transport in the absence of vegetation
                T_char = 5e3;
                if include_veg
                    Thmax = s*q0; %erosion threshold when plants are present
                else
                    Thmax = Th0;
                end
                
                Th = double(Th0+(Thmax-Th0)./T_char.*min(T_growth(r,c), T_char));
                quantity = Qi.*(slps+Cs)-Th;
                quantity = max(quantity, 0);
                Qsi = K*quantity.^m; %RULE 5, Murray and Paola 1997, 
                % ^ Qsi is the amount of sediment transported from the cell 
                %   in question into one of the three downstream immediate neighbours
                Qlost(1, c) = sum(Qsi); %Sediment that is lost from the current cell
                Qlostlong(r,c) = Qlost(1,c); %for mass balance and debugging

                %--------------------------------------------------------------
                %% LATERAL SEDIMENT TRANSFER

                Klmin = 0.06*Kl0;
                Kl = double(Kl0+(Klmin-Kl0)/T_char*min(T_growth(r,c), T_char));
                Qsl = Kl*latslp*Qlost(1,c)./(1-Kl*latslp); 
                % Qsl = is the amount of sediment transported from a 
                %   lateral-neighbour cell into the cell in question
                %   (Murray and Paola 1997, p1006)
                if c == 1
                    Qlost(1,[ncolumns 2]) = Qlost(1,[ncolumns 2])+Qsl;
                    Qlostlat(r,[ncolumns 2]) = Qlostlat(r,[ncolumns 2])+Qsl;
                elseif c == ncolumns
                    Qlost(1,[ncolumns-1 1]) = Qlost(1,[ncolumns-1 1])+Qsl;
                    Qlostlat(r,[ncolumns-1 1]) = Qlostlat(r,[ncolumns-1 1])+Qsl;
                else
                    Qlost(1,[c-1 c+1]) = Qlost(1,[c-1 c+1])+Qsl; 
                    % ^ Add sediment lost to lateral transfer to the total 
                    %       amount of sediment lost from the cell
                    Qlostlat(r,[c-1 c+1]) = Qlostlat(r,[c-1 c+1])+Qsl; 
                    % ^ for mass balance and debugging
                end
                Qgainedlat(r,c) = sum(Qsl); %for mass balance and debugging

                %--------------------------------------------------------------
                %% SAVE WATER AND SEDIMENT TRANSFER TO NEXT ROW
                if c == 1 % left edge case
                    Qnew(1,1:2) = Qnew(1,1:2)+Qi(2:3);
                    Qnew(1,ncolumns) = Qnew(1,ncolumns)+Qi(1);
                    Qsinew(1,1:2) = Qsinew(1,1:2) + Qsi(2:3);
                    Qsinew(1,ncolumns) = Qsinew(1,ncolumns)+Qsi(1);
                elseif c == ncolumns % right edge case
                    Qnew(1,ncolumns-1:ncolumns) = Qnew(1,ncolumns-1:ncolumns)+Qi(1:2);
                    Qnew(1,1) = Qnew(1,1)+Qi(3);
                    Qsinew(1,ncolumns-1:ncolumns) = Qsinew(1,ncolumns-1:ncolumns)+Qsi(1:2);
                    Qsinew(1,1) = Qsinew(1,1)+Qsi(3);
                else
                    Qnew(1, c-1:c+1) = Qnew(1, c-1:c+1) + Qi; %Add transfers to Qnew matrix.
                    Qsinew(1, c-1:c+1) = Qsinew(1, c-1:c+1) + Qsi; %Add transfers to Qsnew matrix
                end
                Qslnew(1, c) = sum(Qsl); %Add lateral transfers to Qsnew matrix

                %--------------------------------------------------------------

                % clear instance variables occasionally
                 if c==ncolumns/2
                     clear c Th0 T_char Th slps latslp slps2 myzeros mynegs quantity;
                 end

            end % End of column loop
            Qgainedlong(r,:) = Qsinew; %for mass balance and debugging
            %         Qnew(1,2) = Qnew(1,2)+Qnew(1,ncolumns); 
            %               % Wrap-around boundary condition for water
            %         Qnew(1,ncolumns-1) = Qnew(1, ncolumns-1)+Qnew(1,1);
            %         Qsnew(1,2) = Qsnew(1,2)+Qsnew(1,ncolumns); 
            %               % Wrap-around boundary condition for sediment
            %         Qsnew(1,ncolumns-1) = Qsnew(1, ncolumns-1)+Qsnew(1,1);

            Q(r+1, :) = Qnew; %Discharges received by the next row
            %         if r>1, z(r,:) = z(r,:)-Qlost+Qslnew; end
            %         z(r+1,:) = z(r+1,:)+Qsinew;
            netsed(r, :) = netsed(r,:) - Qlost + Qslnew; %Take sediment out of 
            %   current row that was transferred away and add sediment from lateral transfer
            netsed(r+1, :) = netsed(r+1,:) + Qsinew; %Transfer sediment to the next row
            %         figure(2), clf, plot(netsed(r,:)), hold on, plot(netsed(r+1,:),'r') %debugging
            %         pause %debugging
        end % End of row loop
        %     netsed(nrows, :) = 0; %No bed elevation change of last row. 
                    % Amount of sediment transport in = amount out.
        %     netsed(1, :) = 0; %No bed elevation change of first row.
        netsed(1,:) = netsed(1,:)+Qsinew; %Puts sediment leaving the bottom of 
            % the model domain onto the top row.
        netsed(nrows,:) = netsed(1,:); 
        % ^ Netsed in the first row also includes the effects of lateral 
        %   erosion, whereas the loop stops before doing this for the last row. 
        %   To ensure the topography changes in the same way for the first and 
        %   last row, this line is needed.

        z = z+netsed; %Change bed elevation as a function of amount of sediment transferred
        %     if max(max(z))>6.5e7+10, pause, end

        %% Determine vegetation distribution:
        if include_veg
            T_resurrect = (1-plants).*T_resurrect+1; 
            % ^ where there are dead plants (which are 0s in the matrix), invert
            % the dead ones as 1s, then add 1 time step.  
            T_growth = plants.*T_growth+1;
            twoday_dep(:,:,1:99) = twoday_dep(:,:,2:100);
            twoday_dep(:,:,100) = netsed;
            dead_plants = union(find(sum(twoday_dep, 3) >=24/5*1e4),... 
                find(sum(twoday_dep,3) <= -24/5*1e4)); % if sediment is being 
            % deposited or eroded above threshold value then plant is dead

            plants(dead_plants) = 0;
            T_resurrect(dead_plants) = 0;
            T_growth(dead_plants) = 0;
        %     new_plants = find(T_resurrect)>=10; % 01/18/18 Colin: values over 10
            %   are reset to 0 when when a plant revives (becomes 1 on plants
            %   matrix and 0 on T_resurrect due to 1-plant)
            new_plants = find(T_resurrect>=10); % revised line
            plants(new_plants) = 1;
        end
        
        %% Save data for discharge and topo movies
        if mod(n, modarg) == 0
            Q_movie(:,:,n/modarg) = Q;
            Topo_movie(:,:,n/modarg) = flipud(fliplr(z-trendz));
        end
        %%
        Q(1,:) = Q(nrows,:);

    end %End of iteration loop


    %% Plot final topography and discharge
    % 2D topo
    zplot = flipud(fliplr(z-trendz)); %Remove the general trend of the data
    figure(1)
    hold off
    subplot(1, 3, 2)
    pcolor(zplot(1:nrows-1, 2:ncolumns-1))
    axis equal
    axis image
    caxis([-Cs Cs]);
    title('Final Topography')
    shading flat
    colormap gray
    colorbar
    caxis([-2e5 2e5])
    % 3D topo
    figure(2)
    subplot(1, 2, 2)
    surfl(z-trendz)
    zlim([-1*zmax, zmax])
    title('Final Topography')
    shading interp

    % Discharge
    figure(1)
    subplot(1,3,3)
    %pcolor(-flipud(fliplr(Q(1:nrows-1, 2:ncolumns-1)))) %Plot the discharge array
    pcolor(flipud(fliplr(Q(1:nrows-1, 2:ncolumns-1)))) %Plot the discharge array
    title('Discharge')
    axis equal
    axis image
    shading flat
    colormap gray
    colorbar



    %% Plot final plant distribution
    figure(3)
    hold off
    subplot(1, 3, 1)
    imagesc(flipud(fliplr(plants)))
    axis equal
    axis image
    title('Plants')
    colormap gray
    colorbar
    caxis([0 1])
    % shading flat
    % colormap gray
    
    %% Save Figures
    % saves each figure in current directory titled by filename argument
    % note: must save in reverse order because if figure 1 is closed,
    %   figure 2 becomes figure 1, figure 3 becomes 2, etc
    
    figure(3)
    fig_3_name = strcat(filename,'_figure_3');
    savefig(fig_3_name)
    close
    
    figure(2)
    fig_2_name = strcat(filename,'_figure_2');
    savefig(fig_2_name)
    close
    
    figure(1)
    fig_1_name = strcat(filename,'_figure_1');
    savefig(fig_1_name)
    close
   
    %% Write videos or save dataset
    discharge_filename = strcat(filename, '_discharge');
    topo_filename = strcat(filename, '_topography');

    if write_videos
        % pad right side with 0s if columns < 64 (for Windows)
        if ncolumns < 64
           Q_movie(:, ncolumns+1:64, :)=0;
           Topo_movie(:,ncolumns+1:64, :)=0;
        end
        % write discharge movie
        file_to_movie(Q_movie, discharge_filename, 4); 
        % file_to_movie(data, filename, figure_number)
        
        % write topo movie
        file_to_movie(Topo_movie, topo_filename, 5);
    else
        % save discharge dataset
        save(sprintf(strcat(discharge_filename, '.mat')), 'Q_movie')
        save(sprintf(strcat(topo_filename, '.mat')), 'Topo_movie')
    end 
    
    close all
    
end

