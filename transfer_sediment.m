function [Qsi, Qlost, quantity] = transfer_sediment(Th0, Thmax, T_growth, ...
    r, c, T_char, Qi, slps, Cs, K, m, Qlost)
    
    % Calculate threshold for sediment entrainment; no sediment transfer if
    % below threshold
    Th = double(Th0+(Thmax-Th0)) * (double(min(T_growth(r,c), T_char))/T_char);
    quantity = Qi.*(slps+Cs)-Th;
    quantity = max(quantity, 0);

    % RULE 5, Murray and Paola 1997
    Qsi = K*quantity.^m; 
    % Sediment that is lost from the current cell
    Qlost(c) = sum(Qsi);
