function [plants, T_growth, T_resurrect, twoday_dep] = ...
    calculate_vegetation(plants, T_growth, T_resurrect, twoday_dep, ...
    netsed, plant_resurrect_time)

    %EROSION_DEPOSITION_THRESHOLD = 24/5*1e4;
    % Where there are dead plants (which are 0s), add 1 in their 
    %   location in T_resurrect to keep track of when they should revive
    T_resurrect = (1-plants).*T_resurrect+1; 

    % grow the living plants by 1 in each iteration
    T_growth = plants.*T_growth+1;

    % cast off the oldest deposition value and add the newest
    twoday_dep(:,:,1:99) = twoday_dep(:,:,2:100);
    twoday_dep(:,:,100) = netsed;

    % If sediment is being deposited or eroded above threshold 
    %   value then plant is dead
    dead_plants = union(find(sum(twoday_dep, 3) >= 24/5*1e4),... 
        find(sum(twoday_dep,3) <= -24/5*1e4)); 

   % kill off the dead plants
    plants(dead_plants) = 0;
    T_resurrect(dead_plants) = 0;
    T_growth(dead_plants) = 0;

    % T_resurrect values over 10 indicate plant
    %   revival; once it's revived in the plants matrix, the
    %   T_resurrect value is reset to 0            
    new_plants = find(T_resurrect>=plant_resurrect_time); % revised line
    plants(new_plants) = 1;