%braided_veg_jf_updated_constants
classdef braided_veg_jf_updated_constants
    properties ( Constant = true )
        s = 50;         % Bed slope -- Original: 100000 -- arbitrary units
        N = 0.5;        % Exponent in water transfer rules
        m = 2.5;        % Exponent in sediment transfer rule
        K = 1E-21;      % Coefficient in sediment transfer rule
        Kl0 = 1E-7;     % Coefficient in lateral sediment transfer rule
        % Th = double(1e9); % s*q0/2;
        Th0 = 1.218e8;  % Threshold stream power needed for sediment transport in the absence of veg.
        % Th0 = s*max(q0)/2;
        T_char = 5e3;   % Threshold growth after which vegetation is fully developed
        % custom colormaps, which correspond to the coloring of Murray and Paola figures
        % green = low values;   red = high values
        green_to_redmap = [linspace(0,0.8,256)', linspace(0.8,0,256)', zeros(256,1)];
        % white = low values;   green = high values;
        greenmap = [linspace(1,0,256)', linspace(1,.5,256)', linspace(1,0,256)'];
    end
end