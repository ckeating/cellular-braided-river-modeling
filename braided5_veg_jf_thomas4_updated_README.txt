NOTE: this information is specific to the function 'braided5_veg_jf_thomas4_updated.m'

Colin Keating
Environmental Systems Dynamics Laboratory
Department of Geography
University of California, Berkeley

%--------------------------------------------------------------------------

ABOUT

This program simulates a braided river channel using a 5-cell cellular 
routing model

%--------------------------------------------------------------------------

MODEL VERIFICATION

*** test with known intial conditions (topography) in order to verify 
discharge, routing, etc rather than random topography

todo:
1. test 5-cell functionality
    - distributing to all 5 downstream cells
    - distributing proportionally to slope
    - no routing to uphill slopes when downhill slope present
    - route inversely proportional to uphill slope when uphill slopes are the only things present



%--------------------------------------------------------------------------

REFERENCES

Murray and Paola, 1994. "A cellular model of braided rivers"

Murray and Paola, 1997. "Properties of a cellular braided-stream model"

Murray and Paola, 2003. "Modelling the effect of vegetation on channel
pattern in bedload rivers"

Thomas and Nicholas, 2002. "Simulation of braided river flow using a 
new cellular routing scheme"
