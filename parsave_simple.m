% Save data within a parfor loop

% from https://www.mathworks.com/matlabcentral/answers/135285-how-do-i-use-save-with-a-parfor-loop-using-parallel-computing-toolbox#answer_202685
function parsave_simple(fname, C)
		save(fname, 'C');
end
