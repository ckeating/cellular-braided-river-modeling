function plot_starting_topography(z, trendz, nrows, ncolumns, green_to_redmap, zmax)
%% PLOT STARTING TOPOGRAPHY
    
    % topo in 2D
    figure(1)
    starttopo = flipud(fliplr(z-trendz));
    subplot(1, 3, 1)
    pcolor(starttopo(1:nrows, 1:ncolumns))
    colormap(green_to_redmap)
    colorbar
    caxis([-2e5 2e5])
    shading flat
    axis equal
    axis image
    title('Starting topography')

    % topo in 3D
    figure(2)
    subplot(1, 4, 1)
    surfl(z-trendz)
    axis 
    title('Starting Topography minus trend')
    shading interp
    zlim([-1*zmax, zmax])
    daspect([1 1 zmax/ncolumns])
    
    % topo in 3D
    figure(2)
    subplot(1, 4, 3)
    surfl(z)
    axis 
    title('Starting Topography including trend')
    shading interp
    zlim([-2*zmax, zmax])
    daspect([1 1 zmax/ncolumns])