ABOUT

This program simulates a braided river channel using a cellular routing 
model described by Murray and Paola (1994, 1997, 2003).  
Does not include modifications described by Thomas et al. (2002).

Note on scaling from Murray and Paola 2003: "For the experiments reported 
here, we assign a cell width of 5 m, so that the model domain is 300 m 
wide, and assign an average slope of 0.01. In this case an iteration 
represents 0.02 days."  

        1 day = 50 iterations
        1 cell = 25 m^2 (cell dimensions 5m x 5m)


%--------------------------------------------------------------------------

USAGE

function braided_veg_jf_updated (nrows, ncolumns, niterations, filename, ...
include_veg, topo_randomness, varargin)

varargin includes:  nframes, Cs, write_videos, Klmin_coeff, ...
   plant_resurrect_time, initial_vegetation_growth, ncells_discharge, ...
   variable_discharge, discharge_timeseries_filename, total_discharge, ...
   save_final_state, final_state_filename

Examples:
braided_veg_jf_updated(10, 10, 1000, "../test_outputs/022718_test1", true, 5,...
    10, 3E5, false, 0.06, 10, 0, 10, false, '', 20000, false, '');
braided_veg_jf_updated(10, 10, 600, "../test_outputs/022718_test2", ...
    true, 10);

Note: for a setup with 300 rows and 50 columns, 1 iteration takes 
approximately 1 second on a typical workstation.  Model runs with
50-100k iterations take 8-24 hours, yet runs of this length are often 
required in order to obtain significant morphological changes to bed 
topography and channel structure.

Parameters:
nrows       (uint32) the number of rows of cells in the model.
            Typical range: 10-300

ncolumns    (uint16) the number of columns of cells in the model
            Typical range: 10-50

niterations     (int32) the number of iterations to run the model.  
                Typical range: 100-150000

filename    (string) the base filename for all graphical and numerical 
            output files for the model run.  Example, if 
            filename="my_output" then figure 1 will be saved as 
            "my_output_figure_1.mat"

include_veg:    (logical) if true, include the effects of vegetation in the 
                model simulation.  If false, do not include the effects of vegetation 

topo_randomness:    (int32) Percent scaling of randomness between cells.  
                    Value of 100 (%) allows randomness on the same order 
                    of the height difference between adjacent rows.

varargin    Includes the following optional arguments. Default values are 
            assumed if any of these are not otherwise specified.  
    
    nframes     (double) Number of stillframes to include in each movie.  
                Default is niterations/10

    Cs          (uint32) Sediment concentration coefficient.  
                Default is 300000 

    write_videos    (logical) If true, call file_to_movie function and 
                    automatically create videos for discharge, topography, 
                    vegetation, vegetation growth. If false, automatically 
                    save the data in matrix-format
                    Default is true

    Klmin_coeff     (double) Coefficient in lateral sediment transfer equation.  
                    Inversely proportional to bank stability.  
                    Default is 0.06

    plant_resurrect_time        (double) Number of iterations it takes for a plant 
                                to come back to life after a disturbance
                                Default is 10
    
    initial_vegetation_growth   (double) Represents maturity of vegetation, 
                                where 5000 is fully matured.  Specifies 
                                initial condition for t_growth matrix
                                Default is 0
 
    num_cells_discharge         (double) Number of cells in top row that discharge 
                                water in the first iteration.  Note: total 
                                discharge is always constant; discharge in 
                                each cell is inversely proportional to 
                                number of cells providing discharge 
                                Default is ncolumns
    
    total_discharge             (double) Total amount of discharge for all
                                cells in the first row.

    save_final_state            (logical) Save the workspace at the end of 
                                iteration, preserving the final state of
                                variables


%--------------------------------------------------------------------------

REFERENCES

Murray and Paola, 1994. "A cellular model of braided rivers"

Murray and Paola, 1997. "Properties of a cellular braided-stream model"

Murray and Paola, 2003. "Modelling the effect of vegetation on channel
pattern in bedload rivers"

