function plot_3D(data, figure_no, fig_title, subplot_rows, ...
    subplot_columns, subplot_no, zmin, zmax, zaspect)

    figure(figure_no)
    subplot(subplot_rows, subplot_columns, subplot_no)
    surfl(data)
    axis 
    title(fig_title)
    shading interp
    zlim([zmin, zmax])
    % daspect([1 1 zmax/ncolumns])
    daspect([1 1 zaspect])
    hold on