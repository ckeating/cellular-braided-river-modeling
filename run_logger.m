function run_logger(nrows, ncolumns, niterations, filename, ...
include_veg, s, N, m, K, Kl0, Th0, T_char, varargin)

% Open text file
fileID = fopen(strcat(filename, '.txt'),'w');

fprintf(fileID, strcat(string(datetime('now')),'\n\n'));
fprintf(fileID, strcat('Run Name: \t', filename,'\n\n'));
fprintf(fileID, strcat('nrows: \t\t', string(nrows),'\n\n'));
fprintf(fileID, strcat('ncolumns: \t', string(ncolumns),'\n\n'));
fprintf(fileID, strcat('niterations: \t', string(niterations),'\n\n'));
fprintf(fileID, strcat('include_veg: \t', string(include_veg),'\n\n'));

% write names of optional arguments if they exist
if ~isempty(varargin)
    varargin = varargin{:};
    [nframes, Cs, write_videos, Klmin_coeff, plant_resurrect_time, ...
        initial_vegetation_growth, num_cells_discharge, variable_discharge, ...
        discharge_timeseries_filename, total_discharge, save_final_state, ...
        load_final_state_filename, include_fixed_topo_condition] = varargin{:};
    fprintf(fileID, strcat('nframes: \t', string(nframes),'\n\n'));
    fprintf(fileID, strcat('Cs: \t\t', string(Cs),'\n\n'));
    fprintf(fileID, strcat('write_videos: \t', string(write_videos),'\n\n'));
    fprintf(fileID, strcat('Klmin_coeff: \t', string(Klmin_coeff),'\n\n'));
    fprintf(fileID, strcat('plant_resurrect_time: \t\t', string(plant_resurrect_time),'\n\n'));
    fprintf(fileID, strcat('intial_vegetation_growth: \t', string(initial_vegetation_growth),'\n\n'));
    fprintf(fileID, strcat('num_cells_discharge: \t\t', string(num_cells_discharge),'\n\n'));
    fprintf(fileID, strcat('variable_discharge: \t\t', string(variable_discharge),'\n\n'));
    fprintf(fileID, strcat('discharge_timeseries_filename: \t', string(discharge_timeseries_filename),'\n\n'));
    fprintf(fileID, strcat('total_discharge: \t\t', string(total_discharge),'\n\n'));
    fprintf(fileID, strcat('save_final_state: \t\t', string(save_final_state),'\n\n'));
    fprintf(fileID, strcat('load_final_state_filename: \t', string(load_final_state_filename),'\n\n'));
    fprintf(fileID, strcat('include_fixed_topo_condition: \t', string(include_fixed_topo_condition),'\n\n'));

    
    fprintf(fileID,'\n\n\n\n');
    
    fprintf(fileID, strcat('Elevation difference between rows: \t\t', string(s),'\n\n'));
    fprintf(fileID, strcat('Exponent in water transfer rules: \t\t', string(N),'\n\n'));
    fprintf(fileID, strcat('Exponent in sediment transfer rule: \t\t', string(m),'\n\n'));
    fprintf(fileID, strcat('Coefficient in sediment transfer rule: \t', string(K),'\n\n'));
    fprintf(fileID, strcat('Coefficient in lateral sediment transfer rule: \t\t\t\t\t', string(Kl0),'\n\n'));
    fprintf(fileID, strcat('Threshold stream power needed for sediment transport in the absence of veg: \t', string(Th0),'\n\n'));
    fprintf(fileID, strcat('Threshold growth after which vegetation is fully developed: \t\t\t\t', string(T_char),'\n\n'));

end