% Cellular model of a braided river
% 3 pixel version with periodic boundary conditions
%
% Run this model using the wrapper function run_models.m
% See braided_veg_jf_updated_README.txt for more information
%
% Returns struct called figure_data which contains all data for making
% figures.

%% TODO List
    
    % ESSENTIALS
    
    % Load from previously saved final state

    % Steps to add Thomas paper functionality (in new file):
        % 1. increase from 3 to 5 cells
        % 2. perform additional calculations for routing potential to each
        % cell
        % 3. calculate net discharge in x and y direction

    % NICE-TO-HAVES
    % Column-wise operation
    % https://www.mathworks.com/company/newsletters/articles/programming-
    % patterns-maximizing-code-performance-by-optimizing-memory-access.html

function graphics_data = braided_veg_jf_updated (nrows, ncolumns, niterations, filename, ...
include_veg, topo_randomness, varargin)
% varargin includes:  nframes, Cs, write_videos, Klmin_coeff, ...
% plant_resurrect_time, initial_vegetation_growth, num_cells_discharge ...
% variable_discharge, discharge_timeseries_filename, total_discharge, ...
% save_final_state, load_final_state_filename,
% include_fixed_topo_condition

% graphics_data output is necessary for enabling function to run in parallel;
% figures are then created in series since threads can't modify graphics
% objects

    %% Handle optional input arguments
    % from https://blogs.mathworks.com/...
    %   loren/2009/05/05/nice-way-to-set-function-defaults/
    % check for correct number of variable arguments
    numvarargs = length(varargin);
    if numvarargs > 13
        error('braided_veg_jf_updated:TooManyOptionalInputs', ...
            'requires at most 13 optional inputs');
    end

    % set defaults for optional inputs
    optional_args = {niterations/10, 300000, true, 0.06, 10, 0, nrows, ...
        false, '', 2000*ncolumns, false, '', false};
    
    % overwrite defaults with user-specified arguments
    optional_args(1:numvarargs) = varargin;
    
    % Assign variable names to all the optional arguments
    [nframes, Cs, write_videos, Klmin_coeff, plant_resurrect_time, ...
        initial_vegetation_growth, ncells_discharge, variable_discharge, ...
        discharge_timeseries_filename, total_discharge, save_final_state, ...
        load_final_state_filename, include_fixed_topo_condition] = optional_args{:};
    
    %% Define Constants
    % from external class
    s = braided_veg_jf_updated_constants.s;
    N = braided_veg_jf_updated_constants.N;         
    m = braided_veg_jf_updated_constants.m;         
    K = braided_veg_jf_updated_constants.K;         
    Kl0 = braided_veg_jf_updated_constants.Kl0;     
    Th0 = braided_veg_jf_updated_constants.Th0;
    T_char = braided_veg_jf_updated_constants.T_char;
    
    % from user parameters
    modarg = niterations/nframes; 
    q0 = total_discharge/ncells_discharge;
    %% Load workspace variables from a previous simulation.
    if ~isempty(load_final_state_filename)
        load(load_final_state_filename);
    else
    %% Initialize variables.
        % DISCHARGE-RELATED
        % Q stores discharge in each cell
        Q = zeros(nrows, ncolumns); 
        % Qi stores discharge to each downstream neighbor, but store as
        % column (Qi(1) = left; Qi(2) = middl; Qi(3) = right) for better performance
        Qi = zeros(3,1);
        
        % Allocate discharge to some or all of the top row of cells
        lower_discharge_rowindex = uint16(ncolumns/2 - ncells_discharge/2 + 1);
        upper_discharge_rowindex = uint16(ncolumns/2 + ncells_discharge/2);
        Q(1,lower_discharge_rowindex:upper_discharge_rowindex)= q0; 
        % Initialize array for calculating deposition that has occurred over 2 days 
        twoday_dep = zeros(nrows, ncolumns, 100); 
        
        % VEGETATION-RELATED
        if initial_vegetation_growth > 0
            % plants stores binary values where 1 indicates presence of a plant 
            % and 0 indicates dead(nonexistent) plant
            plants = uint32(ones(nrows, ncolumns)); 
            % T_growth stores time (# iterations) that each plant has been alive.
            T_growth = uint32(ones(nrows, ncolumns).*initial_vegetation_growth); 
        else
            plants = uint32(zeros(nrows, ncolumns)); 
            T_growth = uint32(zeros(nrows, ncolumns)); 
        end
        % T_resurrect stores time (# iterations) that a plant has been 
        % resurrecting (default 10 iterations before it becomes alive again)
        T_resurrect = uint32(zeros(nrows, ncolumns));
        
        %% Generate starting topography.
        trendz = zeros(nrows, ncolumns);
        % zmax is elevation of the top
        %zmax = s*ncolumns*2; 
        zmax = 1e6;
        zmin = zmax-s*nrows;
        for ii = 1:nrows
            % Put in the general bed slope trend
            trendz(ii, :) = zmax-s*ii; 
        end
        % Put in some topographic white noise on the order of the height 
        % difference between rows.  Avg height difference between rows is 
        % defined by the constant "s" 
        % z = trendz + sign(randn(nrows, ncolumns)) .* 200000 .* rand(nrows, ncolumns); 
        z = trendz + sign(randn(nrows, ncolumns)) .* 50 .* rand(nrows, ncolumns) * topo_randomness/100; 
        
        % make cliff topography (for visual verification of no elevation 
        % change in uppermost or lowermost rows)
        % z(nrows,:) = -16e6;
        
        % for debugging, save first and last row of topography
        first_row_z = z(1,:);
        last_row_z = z(nrows,:);
        
    end
    
    if variable_discharge
        % open some normalized time series of discharge
        discharge_data = open(discharge_timeseries_filename);
        discharge_data = discharge_data.('scaled_discharges');
        % repeat the discharge time series according to number of
        % model iterations
        extended_discharge_data = [repmat(discharge_data, floor(niterations ...
            / numel(discharge_data)), 1); discharge_data(1:mod(niterations, ...
            numel(discharge_data)))];
    end

    % For mass balance and debugging:
    Qlostlat = Q; Qgainedlat = Q; Qlostlong = Q; Qgainedlong = Q; 
    total_sed_quantity=double(0.0);
    
    %% Preallocate space for movies
    Q_movie = zeros(nrows, ncolumns, nframes);      % stores discharge
    Topo_movie = zeros(nrows, ncolumns, nframes);   % stores topography data
    Plant_movie = zeros(nrows, ncolumns, nframes);  % stores plant position data
    Plant_growth_movie = zeros(nrows, ncolumns, nframes);   % stores T_growth data   

    %% Save starting topography
    graphics_data.initial_z = z;
    graphics_data.trendz = trendz;
    graphics_data.zmax = zmax;
    graphics_data.zmin = zmin;
    graphics_data.nrows = nrows;
    graphics_data.ncolumns = ncolumns;
    graphics_data.s = s;
    graphics_data.topo_randomness = topo_randomness;
    
    %% Iteration Process
    for n = uint32(1:niterations)
        
        % test: add discharge in same way to the top each time, instead of
        % routing from the bottom
        Q(1,:)= 0; 
        Q(1,lower_discharge_rowindex:upper_discharge_rowindex)= q0; 
        
        % test: make walls on sides to get rid of Periodic Boundary
        % Condition effect
        z(:,1) = 1e10;
        z(:,ncolumns) = 1e10;
        
        % print occasional iteration, roughly 1 print per second
        if mod(n,15000/(ncolumns*nrows))==0
            n
        end

        % scale first row of discharge: 
        if variable_discharge
            if n==1
                Q(1,:) = Q(1,:) .* extended_discharge_data(n);
            else
                % scale in proportion to previous discharge
                Q(1,:) = Q(1,:) .* ...
                    (extended_discharge_data(n)/extended_discharge_data(n-1));
            end
        end
        
        % Initialize net sediment transfer array
        netsed = zeros(nrows, ncolumns); 
        
        % For mass balance and debugging
        Qlostlat = zeros(nrows,ncolumns); 
        
        % Rows loop.
        for r = uint16(1:nrows-1)
            Qnew = zeros(ncolumns, 1);      % Reset Qnew
            Qsinew = zeros(ncolumns, 1);    % Reset Qsinew
            Qslnew = zeros(ncolumns, 1);    % Reset Qslnew
            % Qlost is the total amount of sediment lost to the three
            % downstream neighbors.
            % Though it nominally stores information about one row, we will
            % store the values in a column to increase computational
            % efficiency.
            Qlost = zeros(ncolumns,1);    
            
            % Columns loop.
            for c = uint8(1:ncolumns)  
                %% Calculate Slopes
                [slps, latslp] = calculate_slopes(r, c, ncolumns, z);
                
                %% Transfer Water
                Qi = transfer_water(slps, Qi, r, c, N, Q);
                  
                % Mass-balance checks
%                 assert(abs(sum(Qi) - Q(r,c)) < 0.00000001,...
%                     'Mass balance warning');
%                 assert(min(Qi) >= 0,...
%                     sprintf('Mass balance warning; min(Qi) is %d', min(Qi)));
                    
                %--------------------------------------------------------------
                %% Transfer Sediment
                if include_veg
                    % Thmax is erosion threshold when plants are present, 2e8
                    %   so about twice the threshold (Th0) in the absence of veg
                    Thmax = s*q0; 
                else
                    Thmax = Th0;
                end
                
                [Qsi, Qlost, quantity] = transfer_sediment(Th0, Thmax, ...
                    T_growth, r, c, T_char, Qi, slps, Cs, K, m, Qlost);
                
                % for mass balance and debugging
                Qlostlong(r,c) = Qlost(c,1);
                total_sed_quantity = total_sed_quantity+quantity;
                
                %% Transfer Sediment Laterally
                [Qsl, Qlost, Qslnew] = transfer_lateral_sediment(Klmin_coeff, ...
                    Kl0, T_growth, Qlost, ncolumns, r, c, latslp, T_char, Qslnew);
                
                % For mass balance and debugging
                Qgainedlat(r,c) = sum(Qsl);

                %% Save water and sediment to next row
                [Qnew,Qsinew] = save_water_sediment(Qi, Qsi, Qnew, Qsinew, ...
                    c, ncolumns);

            end % End of column loop
            
            % for mass balance and debugging
            Qgainedlong(r,:) = Qsinew; 
            
            % check that the sum of next row's discharge is equal to the
            % total discharge
            assert((sum(Qnew) - total_discharge) < 1e-8, ...
                sprintf(['Error: row discharge not equal to total discharge ' , ...
                'total_discharge=%d row discharge=%d' ...
                'iteration=%d row=%d c=%d'], total_discharge, sum(Qnew), n, r, c));

            % Discharges received by the next row
            Q(r+1, :) = Qnew; 
            %         if r>1, z(r,:) = z(r,:)-Qlost'+Qslnew; end
            %         z(r+1,:) = z(r+1,:)+Qsinew;
            
            % Take sediment out of current row that was transferred away 
            % and add sediment from lateral transfer.  (Must transpose
            % Qlost)
            netsed(r,:) = netsed(r,:) - Qlost' + Qslnew';
            % Transfer sediment to the next row
            netsed(r+1, :) = netsed(r+1,:) + Qsinew'; 
           
        end % End of row loop
        
        % assert statement below validates that total sediment transferred is ~0
        sum_sediment_transferred = sum(sum(netsed));
        % TODO: fix net sediment transfer warning
%         assert(abs(sum_sediment_transferred) < 1, ...
%             'sediment mass balance warning: net sediment >= 1');
        

        % Puts sediment leaving the bottom of the model domain onto the top row.
%         netsed(1,:) = netsed(1,:)+Qsinew;
%         netsed(nrows,:) = netsed(1,:);
        % Netsed in the first row also includes the effects of lateral 
        %   erosion, whereas the loop stops before doing this for the last row. 
        %   To ensure the topography changes in the same way for the first and 
        %   last row, this line is needed.

        % No bed elevation change of first or last row (M+P 2003, p.133)  
        if include_fixed_topo_condition
            netsed(1, :) = 0; 
            % netsed(nrows, :) = 0;
        end
        % verify that elevations of first and last rows are not changing
%         assert(isequal(first_row_z, z(1,:)) && isequal(last_row_z, z(nrows,:)), ...
%             'elevation warning: elevation of first and/or last row has changed' )
        
        % Change bed elevation as a function of amount of sediment
        % transferred.
        % Divide by 100 to correct for scaling, so that 
        % elevation changes by at most a few percent of the elevation 
        % difference between rows in each iteration.
        z = z+netsed./100; % for use with small "s"
%         z = z+netsed./4000; % divide by 4000 to correct for scaling

        %% Determine Vegetation Distribution
        if include_veg 
            [plants, T_growth, T_resurrect, twoday_dep] = ...
                calculate_vegetation(plants, T_growth, T_resurrect, ...
                twoday_dep, netsed, plant_resurrect_time);
        end
        
        %% Save data for discharge and topo movies
        if mod(n, modarg) == 0
            Q_movie(:,:,n/modarg) = flipud(fliplr(Q));
            Topo_movie(:,:,n/modarg) = flipud(fliplr(z-trendz));
            Plant_movie(:,:,n/modarg) = flipud(fliplr(plants));
            Plant_growth_movie(:,:,n/modarg) = flipud(fliplr(T_growth));
        end
        %%
        
        % Loop the last row of discharge back as the discharge in the first
        %   row for the next iteration
        Q(1,:) = Q(nrows,:);

    end % End of iteration loop

    
    %% Save final run conditions for plots
    graphics_data.final_z = z;
    graphics_data.final_Q = Q;
    graphics_data.total_discharge = total_discharge;
    graphics_data.final_plants = plants;
    graphics_data.final_T_growth = T_growth;
    graphics_data.filename = filename;

      
%% Save movie data
    graphics_data.discharge_filename = strcat(filename, '_discharge');
    graphics_data.discharge_combined_filename = strcat(filename, '_discharge_combined');
    graphics_data.topo_filename = strcat(filename, '_topography');
    graphics_data.plant_filename = strcat(filename, '_plants');
    graphics_data.plant_growth_filename = strcat(filename, '_plant_growth');
    
    graphics_data.write_videos = write_videos;
    graphics_data.Q_movie = Q_movie;
    graphics_data.Topo_movie = Topo_movie;
    graphics_data.include_veg = include_veg;
    graphics_data.Plant_movie = Plant_movie;
    graphics_data.Plant_growth_movie = Plant_growth_movie;
    graphics_data.max_plant_growth = min(niterations+initial_vegetation_growth,5000);
    
    graphics_data.variable_discharge = variable_discharge;
    graphics_data.niterations = niterations;
    graphics_data.nframes = nframes;
    if variable_discharge
        graphics_data.extended_discharge_data = extended_discharge_data;
    end
    
    % save run-specific parameters to text file
    run_logger(nrows, ncolumns, niterations, filename, include_veg, ...
        s, N, m, K, Kl0, Th0, T_char, varargin);
    
    % save variables
    if save_final_state
        save(strcat(filename, '_final_state'), 'Q', 'Qi', 'twoday_dep', ...
            'plants', 'T_growth', 'T_resurrect', 'z', 'trendz', 'zmax', ...
            'ncolumns', 'nrows');
    end
    
    fprintf('average sediment quantity transferred: %d \n\n', ...
        (sum(total_sed_quantity)/(niterations*ncolumns*nrows)));
    
    fprintf('DONE \n\n');
    
end