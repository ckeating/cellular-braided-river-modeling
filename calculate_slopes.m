function [slps, latslp] = calculate_slopes(r, c, ncolumns, z)
    % Check edge cases to make sure slopes and subsequent 
    %   calculations wrap around from one vertical side to the other
    % Left edge case
    if c == 1
        slps = (z(r,1)-z(r+1,[ncolumns 1 2]))./[sqrt(2) 1 sqrt(2)];
        latslp = z(r,[ncolumns; 2])'-z(r,c);
        latslp = max(0,latslp);
    % Right edge case
    elseif c == ncolumns
        slps = (z(r,ncolumns)-z(r+1,[ncolumns-1 ncolumns 1]))./[sqrt(2) 1 sqrt(2)];
        latslp = z(r,[ncolumns-1; 1])'-z(r,c);
        latslp = max(0,latslp);
    else % General case
        % Slopes for square cells
        % slps = (z(r,c)-z(r+1, c-1:c+1))./[sqrt(2) 1 sqrt(2)]; 
        % faster:
        slps(1) = (z(r,c)-z(r+1, c-1))/sqrt(2); 
        slps(2) = (z(r,c)-z(r+1, c)); 
        slps(3) = (z(r,c)-z(r+1, c+1))/sqrt(2);
        % Lateral slopes
        latslp = z(r, [c-1;c+1])'-z(r,c);
        % Get rid of negative lateral slopes
        latslp = max(0, latslp);                 
    end
    slps = slps'; % transpose to column