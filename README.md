# Cellular Braided River Modeling

The main portion of the package simulates a braided river channel using a cellular routing model described by Murray and Paola (1994, 1997, 2003).  

Separately, the function "braided_veg_jf_thomas4_updated.m" includes modifications described by Thomas et al. (2002).

Environmental Systems Dynamics Laboratory <esdlberkeley.com>

## Getting Started

To run this package in MATLAB, ensure that all files are located in a folder in your current workspace

### Prerequisites

This package requires MATLAB.

UCB Students/Staff: see https://software.berkeley.edu/matlab for info on obtaining a MATLAB license.

### Running simulations

*Note: while the majority of this package supports a function (braided_veg_jf_updated.m) based on Murray and Paola (1994, 1997, 2003), a separate function (braided5_veg_jf_thomas4_updated.m) also exists to run simulations which include modifications from Thomas (2002).* 

For a detailed descrption of the input parameters used with "braided_veg_jf_updated.m" see "braided_veg_jf_updated_README.txt" 

Simulations can be run in two ways:
1. Run a single simulation from the command line by calling the main function

    Examples:

    Run model without specifying optional arguments (uses defaults):

    `braided_veg_jf_updated(10, 10, 1000, "../test_outputs/022718_test1", true);`

    Run model specifying optional arguments:

    `braided_veg_jf_updated(10, 10, 1000, "../test_outputs/022718_test1", true, 10, 3E5, false, 0.06, 10, 0, 10, false, '', 20000, false, '');`

2. Run simulations through a wrapper function (useful for running multiple simulations with different input arguments)

    * example wrapper function is included: "run_models.m"

## Contributors

Laurel Larsen, Josephine Fong, Clothilde Amelie Labrousse, Jennifer Natali, Colin Keating

## References

Murray and Paola, 1994. "A cellular model of braided rivers"

Murray and Paola, 1997. "Properties of a cellular braided-stream model"

Murray and Paola, 2003. "Modelling the effect of vegetation on channel pattern in bedload rivers"

Thomas and Nicholas, 2002. "Simulation of braided river flow using a new cellular routing scheme"
