function discharge_data_parser (filename, field_name, sampling_frequency, varargin)
   
    % Open some cleaned file only containing discharges in single column
    river_data = open(filename);
    % The field name is the name of the file
    discharges=river_data.(field_name);

    % normalize the discharge
    avg_discharge = sum(discharges)/length(discharges)
    scaled_discharges = discharges(1:sampling_frequency:end)./avg_discharge;

    % save normalized discharges to file for use with
    %   braided_veg_jf_updated
    save(sprintf(strcat('scaled_discharges_', field_name, '.mat')), 'scaled_discharges')
    
    % save scaled discharge to a file where you can then make a movie
    %   where the current point is labeled differently
    if length(varargin) > 0
        % initialize videowriter object
        v = VideoWriter(char(filename), 'MPEG-4');
        open(v);
        % get optional inputs - # iterations and # frames in movie
        niterations = varargin{1};
        nframes = varargin{2};
       
        % extend the dataset as necessary
        extended_discharge_data = [repmat(scaled_discharges, floor(niterations ...
            / numel(scaled_discharges)), 1); scaled_discharges(1:mod(niterations, ...
            numel(scaled_discharges)))];
        % sample the scaled dataset 
        sampled_extended_discharge = extended_discharge_data(1:(niterations/nframes):end);
        
        figure();
        hold on
        plot(1:length(sampled_extended_discharge),sampled_extended_discharge);
        title('Scaled Discharge');
        for i = 1:length(sampled_extended_discharge)
            i
            % plot points in black along the discharge graph
            scatter(i,sampled_extended_discharge(i), 'filled', ...
                'MarkerFaceColor', [0 0 0], ...
                'MarkerEdgeColor', [0 0 0]);
            frame=getframe(gcf);
            writeVideo(v,frame);
            % remove most recent feature (the point that was just plotted)
            children = get(gca, 'children');
            delete(children(1));
        end 
    end
close all;
        
        
        