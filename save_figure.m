function save_figure(filename, figure_no)
figure(figure_no)
fig_name = sprintf(strcat(filename,'_figure_%d'),figure_no);
savefig(fig_name)
close