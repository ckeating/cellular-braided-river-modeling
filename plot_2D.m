%% plots inital and final topography and final discharge for 2D plots
function plot_2D(data, figure_no, fig_title, subplot_rows, subplot_columns, subplot_no)
    
    figure(figure_no)
    hold on
    subplot(subplot_rows, subplot_columns, subplot_no)
    pcolor(data)
    axis equal
    axis image
    shading flat
    title(fig_title);
    colorbar