% Create and save all graphics (including figures and movies) associated 
% with a model run.
function create_model_graphics(graphics_data)
    close all
    %% Plot Starting Conditions
    zplot = flipud(fliplr(graphics_data.initial_z - graphics_data.trendz)); 
    plot_2D((zplot), 1, ...
        'Starting Topography minus trendz', 1, 3, 1);
    colormap(braided_veg_jf_updated_constants.green_to_redmap)
    caxis([min(min(graphics_data.initial_z-graphics_data.trendz)) ...
        max(max(graphics_data.initial_z-graphics_data.trendz))])
    %caxis([graphics_data.zmin graphics_data.zmax])
    plot_3D((graphics_data.initial_z-graphics_data.trendz), 2, ...
        'Starting Topography minus trendz', 2, 2, 1, graphics_data.s*-10, ...
        graphics_data.s*10, 10);
    plot_3D((graphics_data.initial_z), 2, ...
        'Starting Topography with trendz', 2, 2, 3, graphics_data.zmin, ...
        graphics_data.zmax, 100);
    
    %% Plot Final Conditions

    % 2D topography
    % Remove the general trend of the topo data
    zplot = flipud(fliplr(graphics_data.final_z-graphics_data.trendz)); 
    %zplot = flipud(fliplr(graphics_data.final_z)); 
    plot_2D(zplot, 1, 'Final Topography minus trendz', 1, 3, 2);
    colormap(braided_veg_jf_updated_constants.green_to_redmap)
    caxis([min(min(graphics_data.initial_z-graphics_data.trendz)) ...
        max(max(graphics_data.initial_z-graphics_data.trendz))])
    %caxis([graphics_data.zmin graphics_data.zmax])

    % 3D topography
    plot_3D((graphics_data.final_z-graphics_data.trendz), 2, ...
        'Final Topography minus trendz', 2, 2, 2, graphics_data.s*-10, ...
        graphics_data.s*10, 10);
    plot_3D((graphics_data.final_z), 2, 'Final Topography with trendz', ...
        2, 2, 4, graphics_data.zmin, graphics_data.zmax, 100);

    % Discharge
    plot_2D(flipud(fliplr(graphics_data.final_Q(1:graphics_data.nrows,1:graphics_data.ncolumns))), ...
        1, 'Final Discharge', 1,3,3);
    reversed_gray = flipud(gray);
    colormap(gca, reversed_gray)
    % colormap(gca, 'gray')
    % caxis([graphics_data.total_discharge/graphics_data.ncolumns ...
    %         graphics_data.total_discharge/6])
    caxis([graphics_data.total_discharge/graphics_data.ncolumns ...
            graphics_data.total_discharge/10])
        
    % Final plant distribution
    plot_2D(flipud(fliplr(graphics_data.final_plants)),3, 'Final Plants', 1,3,1); 
    colormap(braided_veg_jf_updated_constants.greenmap);
    caxis([0 1])
    
    % Final T_growth
    plot_2D(flipud(fliplr(graphics_data.final_T_growth)), 3, 'Final T\_growth', 1,3,3);
    colormap(braided_veg_jf_updated_constants.greenmap);
    
    %% Save Figures
    % Must save in reverse order because if figure 1 is closed,
    %   figure 2 becomes figure 1, figure 3 becomes 2, etc
    for figure_i=3:-1:1
        save_figure(graphics_data.filename, figure_i);
        pause(0.5)
    end
    close all
    
    %% Write and Save Movies
    if graphics_data.write_videos
        % manual plot dimensions: 1120 width by 840 height
        set(gca(), 'XLimMode', 'manual', 'YLimMode', 'manual');
        
        % write discharge movie
        file_to_movie(graphics_data.Q_movie, graphics_data.discharge_filename, ...
            reversed_gray, graphics_data.total_discharge/graphics_data.ncolumns, ...
            graphics_data.total_discharge/graphics_data.ncolumns*4);
        
        % write topo movie
        file_to_movie(graphics_data.Topo_movie, graphics_data.topo_filename, ...
            braided_veg_jf_updated_constants.green_to_redmap, -100, 100);
        
        if graphics_data.include_veg
            % write vegetation movie
            file_to_movie(graphics_data.Plant_movie, graphics_data.plant_filename, ...
                braided_veg_jf_updated_constants.greenmap, 0, 1);

            % write T_growth movie
            file_to_movie(graphics_data.Plant_growth_movie, ...
                graphics_data.plant_growth_filename, ...
                braided_veg_jf_updated_constants.greenmap, 0, ...
                graphics_data.max_plant_growth);
        end
        
        % write movie with cell discharge and discharge plot side-by-side
        if graphics_data.variable_discharge
            file_to_movie_discharge(graphics_data.Q_movie, ...
                graphics_data.discharge_combined_filename, ...
                4, 'gray', graphics_data.niterations, ...
                graphics_data.nframes, graphics_data.extended_discharge_data);
        end
        
    else
        % save discharge, topo, & plant datasets to .mat files
        save(sprintf(strcat(graphics_data.discharge_filename, '.mat')), '_frames')
        save(sprintf(strcat(graphics_data.topo_filename, '.mat')), '_frames')
        save(sprintf(strcat(graphics_data.plant_filename, '.mat')), '_frames')
        save(sprintf(strcat(graphics_data.plant_growth_filename, '.mat')), '_frames')    
    end   