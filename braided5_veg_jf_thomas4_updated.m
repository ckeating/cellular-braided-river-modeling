%% Cellular model of a braided stream with vegetation
% 5 pixel version with periodic boundary conditions

clear
clc
close all

%------------------------------------------------------
%% INPUT VARIABLES

nrows = 50;         % # of rows        initial:300
celldim = 0.2;      % Cell dimensions  <--  not used
ncolumns = 50;      % # of columns     initial: 50 and required elsewhere to be 50
niterations = 20;   % # of iterations
nframes = niterations/10;       % # of frames to save in a movie.
ncellsdischarge = 5;            % # of cells at the top row receiving discharge -- Original: 25
q0 = .2;            % Discharge in each cell receiving discharge in m^3/s
s = 0.01;           % Bed slope (50*q0 to keep Murray and Paola slope/discharge ratio)

% q0(round(ncolumns/2-ncellsdischarge/2)+1:round(ncolumns/2-ncellsdischarge/2)+ncellsdischarge) = 25000;
% q0 = 10000*[0 0 ones(1,ncolumns-4) 0 0]; % Initial amount of water in top row cells

N = 0.5;            % Exponent in water transfer rules
m = 2.5;            % Exponent in sediment transfer rule
K = 1E-21;          % Coefficient in sediment transfer rule
Cs = 300000;        % Sediment concentration that's going to get carried on. Allows sediment to be carried uphill
Kl0 = 1E-7;         % Coefficient in lateral sediment transfer rule
Th = 1e9;           % s*q0/2;
D = 0.6;            % Coefficient in depth-flow equation
a = 1;              % Empirical constant used in sed tspt capacity eq.
b = 1.5;            % Empirical cst used in sed tspt capacity eq.
lam = 0.1;
CStrP=0.05;         % Sediment entrainment threshold based on scheme of Reinfelds and Nanson (Thomas et al.)
omega=0.12;         % Parameter that represents the effects of inertia on sediment transport
k = 0.3;            % Parameter varying between 0 and 1, used to define extent to which sed tspt rates are influenced by TCAPi
E = 0.2;            % Constant that represents bank erodibility

%------------------------------------------------------
%% CALCULATE STUFF FROM INPUT VARIABLES

modarg = niterations/nframes; %Argument for modulus

%------------------------------------------------------
%% INITIALIZE VARIABLES

Q = zeros(nrows, ncolumns);                 % Discharge
Qi = zeros(1, 5);                           % Qi= discharge received by each of the downstream cells from the upstream cell
Q_movie = zeros(nrows, ncolumns, nframes);  % Q_movie stores Q data to make a movie (in separate script)
trendz = zeros(nrows, ncolumns);            % 
plants = zeros(nrows, ncolumns);            % stores 1 if the cell has a plant; 0 if no plant
T_growth = zeros(nrows, ncolumns);          % stores ints representing # iterations a plant has been alive
T_resurrect = zeros(nrows, ncolumns);       % stores ints representing germination period before plant 
                                            % revival; once threshold value is reached, plant resurrects 
% for mass balance and debugging
Qlostlat = Q; 
Qgainedlat = Q; 
Qlostlong = Q; 
Qgainedlong = Q; 

dslope = zeros(nrows,ncolumns);     % downstream slope
slopesum = zeros(nrows,1);          % sum of slopes of wet cells in each row
slopeav = zeros(nrows,1);           % averaged slopes in each row
qcount = zeros(nrows,ncolumns);     % counts wet cells
StrP = zeros(nrows,ncolumns);       % unit stream power for entrainment of sediment
P = zeros(1,5);                     % stream routing potential
QsMat = zeros(nrows,ncolumns,5);    % create a (m x n x 5) matrix of sed 
                                    %   discharge from 5 upstream neighbors
QsMat(1,:,:) = q0;
TACT0 = zeros(1,5); % rate of incoming sed tspt to cell
TCAPi = zeros(1,5); % sed tspt capacity along pathway
TSUPi = zeros(1,5); % potential rate of sed supply to the pathway from 
                    %   upstream cell in the absence of erosion and 
                    %   deposition in that cell
TPi = zeros(1,5);   % transport potential

%------------------------------------------------------
%% COME UP WITH STARTING TOPOGRAPHY

% Elevation of the top
zmax = s*nrows*2; 
for ii = 1:nrows
    % Put in the general bed slope trend
    trendz(ii, :) = zmax-s*ii; 
end
% Put in some white noise on the order of the height difference between rows. 
z = trendz + sign(randn(nrows, ncolumns)).*200000.*rand(nrows, ncolumns); 

% NEEDED: M+P used initial topography perturbations with amplitude of 
%   200,000. Use that instead of s/2?

% makes bottom of domain same as top corrected for elevation differences
z(1,:) = trendz(1,:)+z(nrows,:)-trendz(nrows,:); 
% Thomas et al.
f = sqrt(s)/N;              % Froude number
h = zeros(nrows, ncolumns); % Initialize depth flow
h(1,:) = (q0/f)^D;          % Flow depth at starting row
% or h(1,:) = (q0/(K*S^a)).^(1/b)

%------------------------------------------------------
%% PLOT STARTING TOPOGRAPHY
starttopo = flipud(fliplr(z-trendz));
subplot(1, 3, 1)
pcolor(starttopo(1:nrows-1, 2:ncolumns-1))
colormap gray
shading flat
axis equal
axis image
title('Starting topography')

%------------------------------------------------------
%% PROCESS
% ********** FLAGGED **********
% Find the lowest cells in the second row and allocate discharge to them
[trash, I] = sort(z(2,:)); 

% Q(1,:) = 0;
% Q(1,I(1:ncellsdischarge)) = q0;
% Q(1,1:12)= 0;
% Q(1,13:38)=q0; 
% Q(1,39:50)=0;

% Adds initial discharge to entire top row
Q(1,:)= q0; 

% Initialize array for calculating deposition that has occurred over 2 days
twoday_dep = zeros(nrows, ncolumns, 100);   
for n = 1:niterations
    % print iteration
    n 
    % Initialize net sediment transfer array
    netsed = zeros(nrows, ncolumns); 
    % For mass balance and debugging
    Qlostlat = zeros(nrows,ncolumns); 
    
    % maximum possible dropoff from row to row
%     D = max(max(max(max(max(z(1:nrows-1,2:ncolumns)-z(2:nrows,1:ncolumns-1))),...
%         max(max(z(1:nrows-1,1:ncolumns-1)-z(2:nrows,2:ncolumns))))/sqrt(2), ...
%         max(max(max(z(1:nrows-1,2:ncolumns)-z(2:nrows,1:ncolumns-1))),...
%         max(max(z(1:nrows-1,1:ncolumns-1)-z(2:nrows,2:ncolumns)))))/sqrt(5), ...
%         max(max(z(1:nrows-1,:)-z(2:nrows,:)))); 
    %     K = min(Kdef, D/2*(min(min((Q.*(D+Cs)-Th).^-m)))); 
    %     % Set the sediment transport coefficient so that stability is 
    %     %   achieved. This is like dynamic timestepping.
    %     z = [z(:,ncolumns-1) z(:,2:ncolumns-1) z(:,2)]; % Wraps boundary
    %     K = Kdef; % Use this instead of stability condition K from now on
    for r = 1:nrows-1

        % plot discharge
        
%       discharge = flipud(fliplr(Q));
        discharge = Q;
        figure(2)
        pcolor(discharge)
        colormap gray
        shading flat
        axis equal
        axis image
        title(strcat('Discharge, n=', num2str(n), ' r= ', num2str(r)))
        
        h_new = zeros(1,ncolumns); % stores flow depths for following row, for updating h
        
        %Qi = zeros(1, ncolumns);
        Qnew = zeros(1, ncolumns);      %Reset Qnew
        Qsinew = zeros(1, ncolumns);    %Reset Qsinew
        Qslnew = zeros(1, ncolumns);    %Reset Qslnew
        Qlost = zeros(1, ncolumns);     %Reset Qlost        
         
        %% WATER DISCHARGE COLUMN LOOP                  
        for cc = 1:ncolumns
               
            %% STRICTLY DOWNSTREAM SLOPE
            if n==1     % only need to calculate once
                if r==1 % wrap around
                    dslope(r,cc)=z(nrows,cc)-z(r,cc);
                else
                    dslope(r,cc)=z(r-1,cc)-z(r,cc);
                end
            end
            %% CALCULATE FLOW DEPTHS, Thomas et al. Eq.(4)
            
            if cc==1
                flow = h(r,1)-z(r,1) - ...
                    z(r+1,[ncolumns-1 ncolumns 1 2 3])- ... 
                    ([sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)].*s);
                flow = max(0,flow);
            elseif cc==2
                flow = h(r,2)-z(r,2) - ...
                    z(r+1,[ncolumns 1 2 3 4])- ... 
                    ([sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)].*s);
                flow = max(0,flow);
            elseif cc == ncolumns-1
                flow = h(r,ncolumns-1)-z(r,ncolumns-1)- ... 
                    z(r+1,[ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 1])- ...
                    ([sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)].*s);
                flow = max(0,flow);
            elseif cc == ncolumns
                flow = h(r,ncolumns)-z(r,ncolumns)- ... 
                    z(r+1,[ncolumns-2 ncolumns-1 ncolumns 1 2])- ...
                    ([sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)].*s);
                flow = max(0,flow);
            else
                flow = h(r,cc)+z(r,cc)-z(r+1,[cc-2 cc-1 cc cc+1 cc+2])- ...
                    ([sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)].*s);
                flow = max(0,flow);
            end
              
        %-------------------------------------------------------------
            %% CALCULATE SLOPES
            % Since each cell in the top row receives discharge, make sure
            %   slopes and subsequent calculations wrap around like with
            %   elevation.
            if cc == 1
                slps = (z(r,1) - ...
                    z(r+1,[ncolumns-1 ncolumns 1 2 3]))./ ...
                    [sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                latslp = z(r,[ncolumns-1 ncolumns 2 3])-z(r,cc);
                latslp = max(0,latslp);                
            elseif cc == 2
                slps = (z(r,ncolumns) - ...
                    z(r+1,[ncolumns 1 2 3 4]))./ ...
                    [sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                latslp = z(r,[ncolumns 1 3 4])-z(r,cc);
                latslp = max(0,latslp);                
            elseif cc == ncolumns-1
                slps = (z(r,ncolumns) - ...
                    z(r+1,[ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 1]))./ ...
                    [sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                latslp = z(r,[ncolumns-3 ncolumns-2 ncolumns 1])-z(r,cc);
                latslp = max(0,latslp);
            elseif cc == ncolumns
                slps = (z(r,ncolumns)- ...
                    z(r+1,[ncolumns-2 ncolumns-1 ncolumns 1 2]))./ ...
                    [sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)];
                latslp = z(r,[ncolumns-2 ncolumns-1 ncolumns 2])-z(r,cc);
                latslp = max(0,latslp);              
            else 
                % Slopes for square cells
                slps = (z(r,cc)-z(r+1, cc-2:cc+2))./ ...
                    [sqrt(5) sqrt(2) 1 sqrt(2) sqrt(5)]; 
                % Lateral slopes
                latslp = z(r, [cc-2 cc-1 cc+1 cc+2])-z(r,cc); 
                % Get rid of negative lateral slopes
                latslp = max(0, latslp); 
            end
               
            %% CALCULATE DISCHARGE using Thomas et al.
            % If at least one downstream cell has positive flow
            if max(flow)>0 
                
                flowzeros = find(flow<0);
                flow(flowzeros)=0;
                
                % Downstream routing potential, Eq. (5), Thomas, p.181
                P=K*((flow).^b).*(abs(slps)).^m;
            
            % If no downstream cells have positive flow
            elseif max(flow)<=0 
                % Maximum flow depth, Eq. (6), Thomas, p.182
                hmax = (Q(r,cc)/sqrt(9.806)).^0.67; 
                if cc==1
                    flow = hmax+min(z(r+1, ...
                        [ncolumns-1 ncolumns 1 2 3]))- ...
                        z(r+1, [ncolumns-1 ncolumns 1 2 3]);
                elseif cc==2
                    flow = hmax+min(z(r+1, ...
                        [ncolumns 1 2 3 4]))- ... 
                        z(r+1, [ncolumns 1 2 3 4]);                    
                elseif cc==ncolumns-1
                    flow = hmax+min(z(r+1, ...
                        [ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 2]))- ... 
                        z(r+1,[ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 1]);                    
                elseif cc==ncolumns
                    flow = hmax+min(z(r+1, ...
                        [ncolumns-2 ncolumns-1 ncolumns 1 2]))- ... 
                        z(r+1,[ncolumns-2 ncolumns-1 ncolumns 1 2]);                    
                else
                    flow = hmax+min(z(r+1, ...
                        [cc-2 cc-1 cc cc+1 cc+2]))- ... 
                        z(r+1, [cc-2 cc-1 cc cc+1 cc+2]);
                    
                    
                    % Downstream routing potential, Eq. (8), Thomas, p.182
                    %P=flow.^1.5; 
                end 
                % set any remaining negative flow depths to 0 (per Eq. 7,
                %   Thomas)
                flow = max(0,flow);
                P = flow.^1.5; 
            end 
            
            % check if flow is 0 to all downstream cells, if so, then set
            %   Qi to 0s
            if sum(flow) == 0
                Qi = zeros(1,5);
            else
                Qi = Q(r,cc).*P(1:5)/sum(P); % ERROR when sum(P)=0 or sum
            end
              
            %% SAVE WATER DISCHARGE
            if cc == 1
                Qnew(1,1:3) = Qnew(1,1:3) + Qi(3:5);
                Qnew(1,ncolumns-1:ncolumns) = Qnew(1,ncolumns-1:ncolumns) + Qi(1:2);
            elseif cc == 2
                Qnew(1,ncolumns) = Qnew(1,ncolumns) + Qi(1);
                Qnew(1,1:4) = Qnew(1,1:4) + Qi(2:5);
            elseif cc==ncolumns-1
                Qnew(1,ncolumns-3:ncolumns) = Qnew(1,ncolumns-3:ncolumns) + Qi(1:4);
                Qnew(1,1) = Qnew(1,1) + Qi(5);
            elseif cc==ncolumns
                Qnew(1,ncolumns-2:ncolumns) = Qnew(1,ncolumns-2:ncolumns) + Qi(1:3);
                Qnew(1,1:2) = Qnew(1,1:2) + Qi(4:5);
            else
                % Add transfers to Qnew matrix.
                Qnew(1, cc-2:cc+2) = Qnew(1, cc-2:cc+2) + Qi; 
            end
            
            % Discharges received by the next row
            Q(r+1,:) = Qnew; % nan problem: conditional breakpoint set as: sum(isnan(Qnew))>0
            % only count slopes of wet cells
%             if Qnew(r,cc)>0 
            if Qnew(1,cc)>0
                slopesum(r,1) = slopesum(r,1) + dslope(r,cc);
                qcount(r,cc) = 1;
            end
            
            %% CALCULATE FLOW DEPTH AT NEXT ROW (from next row discharge)
            h_new = Qnew./f.^D; % Equation 2, Thomas, p181 (also see bottom of p.182)
            h(r+1,:) = h_new;
            % Discharges received by the next row
            Q(r+1,:) = Qnew; % nan problem: conditional breakpoint set as: sum(isnan(Qnew))>0
            % only count slopes of wet cells
%             if Qnew(r,cc)>0 
            if Qnew(1,cc)>0
                slopesum(r,1) = slopesum(r,1) + dslope(r,cc);
                qcount(r,cc) = 1;
            end
        end
             
        %------------------------------------------------------------%
        %% SEDIMENT DISCHARGE COLUMN LOOP
        for c = 1:ncolumns
                
            %% CALCULATE AVERAGE SLOPES
            q=sum(qcount(r,:)); % calculates the number of wet cells in row r
            if mod(q,2) ~= 0
                q=q+1;
            end
            qq = q-r;
            qq2 = nrows-r;
            if r-q/2 > 0 % smoothed slope over #rows=floodplain width
                if r+q/2 <= nrows
                    avgslope = (sum(slopeav((r-q/2):(r+q/2),1)))/q;
                else %wrap around
                    avgslope = (sum(slopeav((r-q/2):nrows,1)) + sum(slopeav(1:(q/2-qq2),1))) / q;
                    % avgslope=sum(slopeav((r-q/2):nrows,1)+slopeav(1:(q/2-qq2),1))/q;
                end
            elseif r-q/2 < 0 % wrap around
                if r+q/2 > nrows
                    avgslope = (...
                        sum(slopeav((1:nrows),1)) + ...
                        sum(slopeav((1:q/2-qq2),1)) + ... 
                        sum(slopeav((nrows-qq):nrows,1))...
                        )/q;
                else
                    avgslope = (...
                        sum(slopeav(((nrows-qq):nrows),1)) + ...
                        sum(slopeav(1:(r+q/2),1))...
                        )/q;               
                end
            end            
                
            %% CALCULATE STREAM POWER
            newslope = lam.*slps+(1-lam).*avgslope;
            if c==1
                StrP(r,[ncolumns-1 ncolumns 1 2 3]) = Q(r,[ncolumns-1 ncolumns 1 2 3]).*newslope(1,:);
            elseif c==2
                StrP(r,[ncolumns 1 2 3 4]) = Q(r,[ncolumns 1 2 3 4]).*newslope(1,:);
            elseif c==ncolumns-1
                StrP(r,[ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 1]) = ...
                    Q(r,[ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 1]).*newslope(1,:);
            elseif c==ncolumns
                StrP(r, [ncolumns-2 ncolumns-1 ncolumns 1 2])= ...
                    Q(r,[ncolumns-2 ncolumns-1 ncolumns 1 2]).*newslope(1,:);
            else
                % Unit stream power, Eq. (10) (reference?) 
                % https://en.wikipedia.org/wiki/Stream_power
                StrP(r,[c-2 c-1 c c+1 c+2]) = Q(r,[c-2 c-1 c c+1 c+2]).*newslope(1,:); 
            end
            
            %% CALCULATE SEDIMENT TRANSPORT
            % 01/16/18 On the first and last two columns, some of the downstream neighbors 
            %   wrap around to the columns on the opposite side of the stream.
           
            %         | | |x| | |  where x=current cell
            %         |1|2|3|4|5|  where 1-5 are downstream neighbor cells  
            

            if c==1
%                 TACT0(1,1:5) = QsMat(r,c,[ncolumns-1 ncolumns 1 2 3]); 
                % ^ ORIGINAL LINE
                TACT0(1,3:5) = QsMat(r, c, [3 4 5]); 
                TACT0(1,1) = QsMat(r, ncolumns-1, 3);
                TACT0(1,2) = QsMat(r, ncolumns, 3); % last column, cell directly above

              % Current error on original line above because index of
              % ncolumns and ncolumns-1 will generally exceed the
              % maximum value (5) in the third dimension of the
              % matrix.  Eg QsMat(1,1,[49 50 1 2 3]) tries to first
              % return:
              % (:,:,49), which doesn't exist since there are only
              % 5 spots in the 3rd dimension

            %                       TACT0(1,1:5) = QsMat(r,c,[nthird-1 nthird 1 2 3]);
            %                       TACT0(1,1:5) = QsMat(r,[ncolumns-1 ncolumns 1 2 3], ?);
            %                       TACT0(1,1:5) = reshape(QsMat(r,c,:),1,5);

            elseif c==2
              % TACT0(1,1:5) = QsMat(r,c,[ncolumns 1 2 3 4]);
              % ^ ORIGINAL LINE
                TACT0(1,2:5) = QsMat(r, c, [2 3 4 5]);  
                TACT0(1,1) = QsMat(r, ncolumns, 3); % last column, cell directly above

            elseif c==ncolumns-1
              % TACT0(1,1:5) = QsMat(r,c,[ncolumns-3 ncolumns-2 ncolumns-1 ncolumns 1]);
              % ^ ORIGINAL LINE

                TACT0(1,1:4) = QsMat(r, c, [1 2 3 4]);
                TACT0(1,5) = QsMat(r, 1, 3); % first column, cell directly above
            elseif c==ncolumns
              % TACT0(1,1:5) = QsMat(r,c,[ncolumns-2 ncolumns-1 ncolumns 1 2]);
              % ^ ORIGINAL LINE

                TACT0(1,1:3) = QsMat(r, c, [1 2 3]);
                TACT0(1,4) = QsMat(r, 1, 3); % 1st column, cell directly above
                TACT0(1,5) = QsMat(r, 2, 3); % 2nd column, cell directly above
            else
              % TACT0(1,1:5) = QsMat(r,c,c-2:c+2);
              % ^ ORIGINAL LINE
              % ^actual rate of sediment transport entering the 
              % upstream cell in the direction parallel to the 
              % downstream flow routing pathway under consideration
              % (Eq. 13), 

                TACT0(1,1:5) = QsMat(r,c,1:5);
            end
              
            
            %-------------------------------------------------------------
            %% TRANSFER WATER
            % if max(slps)>0 %If there is at least 1 positive slope
            %     slps2 = max(0, slps); 
            %     % Reset negative slopes to 0. NEEDED: Don't reset this 
            %     %   variable, since actual slope determines sediment transfers below!!
            %     Qi = Q(r,c).*(slps2.^N)/sum(slps2.^N); %RULE 1A. Gives array of 3 transfers
            % elseif max(slps)<0 %If there are only negative slopes
            %     slps2 = abs(slps); %Take the absolute value of the slopes
            %     Qi = Q(r,c)./(slps2.^N.*sum(1./slps2.^N)); %RULE 1B.
            % else %There is a combination of negatives and zeros
            %     myzeros = find(slps >= 0);
            %     % pause
            %     mynegs = find(slps < 0);
            %     Qi(myzeros) = Q(r,c)./length(myzeros);
            %     Qi(mynegs) = 0;
            % end %End of slopes if statement
            % if abs(sum(Qi) - Q(r,c))>0.00000001, pause, end
            % if min(Qi)<0, pause, end
            %             end
            %--------------------------------------------------------------
            %% TRANSFER SEDIMENT.
            
            Th0 = 1.218e5; %s*max(q0)/2; %Threshold in the absence of vegetation
            T_char = 5e3;
            Thmax = s*q0; %erosion threshold when plants are present
            Th = Th0+(Thmax-Th0)./T_char.*min(T_growth(r,c), T_char);
            quantity = Qi.*(slps+Cs)-Th;
            quantity = max(quantity, 0);
            Qsi = K*quantity.^m;            % RULE 5, Murray and Paola 1997  % NOTE: line originally commented out
            Qlost(1, c) = sum(Qsi);         % Sediment that is lost from the current cell
            Qlostlong(r,c) = Qlost(1,c);    % for mass balance and debugging
            % 1/17/18 Below: Yields matrix of complex numbers
            TCAPi(1,1:5) = a*(max((StrP(1,1:5)-CStrP),0)).^b; 
            % 01/23/18 ^ FIXME: TCAPi yields complex values; should
            %   probably not allow (StrP(1,1:5)-CStrP) to be <0
            
            % TCAPi = Sediment transport capacity per unit width along each flow pathway leaving the upstream cell
            %   
            TPi(1,1:5) = TCAPi(1,1:5) + omega.*TACT0(1,1:5);
            TSUPi(1,1:5) = (QsMat(r,c,1:5)./sum(QsMat(r,c,:)));%.*TPi(1,1:5);
            Qsi = k.*TCAPi+(1-k).*TSUPi; %called TiACT in Eq. 12
            % 01/23/18 Colin: 
            
            %--------------------------------------------------------------
            %% LATERAL SEDIMENT TRANSFER
            
            Klmin = 0.06*Kl0;
            Kl = Kl0+(Klmin-Kl0)/T_char*min(T_growth(r,c), T_char);
            %Qsl = Kl*latslp*Qlost(1,c)/(1-Kl*latslp); %Lateral sediment transfer received from the two neighboring cells
            Qsl = E*latslp.*Qsi([1 2 4 5]);
            %Qsl = E.*latslp*Qsi; %(Eq. 14)  Colin: original line
            %   Qsl is lateral sediment transfer
            % 01/23/18 - Colin - FIXME: the above line now passes, but is it correct?
            %   Need to verify Eq 14, but what paper is it from? (definitely not
            %   Murray)

            if c==1
                %Qlost(1,[ncolumns-1 ncolumns 1 2 3]) = Qlost(1,[ncolumns-1 ncolumns 1 2 3]) + Qsl;
                Qlost(1,[ncolumns-1 ncolumns 2 3]) = Qlost(1,[ncolumns-1 ncolumns 2 3])+Qsl;
                %Qlostlat(r,[ncolumns-1 ncolumns 1 2 3]) = Qlostlat(r,[ncolumns-1 ncolumns 1 2 3]) + Qsl;
                Qlostlat(r,[ncolumns-1 ncolumns 2 3]) = Qlostlat(r,[ncolumns-1 ncolumns 2 3]) + Qsl;
                % ^ In both cases the current cell it can't lose sediment from itself so
                % it shouldn't be included
            elseif c==2
                Qlost(1,[ncolumns 1 3 4]) = Qlost(1,[ncolumns 1 3 4]) + Qsl;
                Qlostlat(r,[ncolumns 1 3 4]) = Qlostlat(r,[ncolumns 1 3 4]) + Qsl;
            elseif c==ncolumns-1
                Qlost(1,[ncolumns-3 ncolumns-2 ncolumns 1]) = Qlost(1,[ncolumns-3 ncolumns-2 ncolumns 1]) + Qsl;
                Qlostlat(r,[ncolumns-3 ncolumns-2 ncolumns 1]) = Qlostlat(r,[ncolumns-3 ncolumns-2 ncolumns 1]) + Qsl;
            elseif c==ncolumns
                Qlost(1,[ncolumns-2 ncolumns-1 1 2]) = Qlost(1,[ncolumns-2 ncolumns-1 1 2]) + Qsl;
                Qlostlat(r,[ncolumns-2 ncolumns-1 1 2]) = Qlostlat(r,[ncolumns-2 ncolumns-1 1 2])+Qsl;
            else
                Qlost(1,[c-2 c-1 c+1 c+2]) = Qlost(1,[c-2 c-1 c+1 c+2])+Qsl; 
                % ^ Add sediment lost to lateral transfer to the total amount of sediment lost from the cell
                Qlostlat(r,[c-2 c-1 c+1 c+2]) = Qlostlat(r,[c-2 c-1 c+1 c+2])+Qsl; %for mass balance and debugging
            end
            Qgainedlat(r,c) = sum(Qsl); %for mass balance and debugging
            
            %--------------------------------------------------------------
            %% SAVE SEDIMENT TRANSFER TO NEXT ROW
            if c == 1              
                Qsinew(1,1:3) = Qsinew(1,1:3) + Qsi(3:5);
                Qsinew(1,ncolumns-1:ncolumns) = Qsinew(1,ncolumns-1:ncolumns)+Qsi(1:2);
                QsMat(r,c,1:2) = Qsinew(1,ncolumns-1:ncolumns);
                QsMat(r,c,3:5) = Qsinew(1,1:3);
            elseif c == 2                
                Qsinew(1,ncolumns) = Qsinew(1,ncolumns) + Qsi(1);
                Qsinew(1,1:4) = Qsinew(1,1:4)+Qsi(2:5);
                QsMat(r,c,1) = Qsinew(1,ncolumns);
                QsMat(r,c,2:5) = Qsinew(1,1:4);
            elseif c== ncolumns-1
                Qsinew(1,ncolumns-3:ncolumns) = Qsinew(1,ncolumns-3:ncolumns)+Qsi(1:4);
                Qsinew(1,1) = Qsinew(1,1)+Qsi(5);
                QsMat(r,c,1:4) = Qsinew(1,ncolumns-3:ncolumns);
                QsMat(r,c,5) = Qsinew(1,1);
            elseif c == ncolumns                
                Qsinew(1,ncolumns-2:ncolumns) = Qsinew(1,ncolumns-2:ncolumns)+Qsi(1:3);
                Qsinew(1,1:2) = Qsinew(1,1:2)+Qsi(4:5);
                QsMat(r,c,1:3) = Qsinew(1,ncolumns-2:ncolumns);
                QsMat(r,c,4:5) = Qsinew(1,1:2);
            else
                Qsinew(1, c-2:c+2) = Qsinew(1, c-2:c+2) + Qsi; %Add transfers to Qsnew matrix.
                QsMat(r,c,1:5)=Qsinew(1,c-2:c+2); %Save incoming sediment transfers
          
            end
            Qslnew(1, c) = sum(Qsl); %Add lateral transfers to Qsnew matrix

            %--------------------------------------------------------------
            
        end %End of column loop
        Qgainedlong(r,:) = Qsinew; %for mass balance and debugging
        %         Qnew(1,2) = Qnew(1,2)+Qnew(1,ncolumns); %Wrap-around boundary condition for water
        %         Qnew(1,ncolumns-1) = Qnew(1, ncolumns-1)+Qnew(1,1);
        %         Qsnew(1,2) = Qsnew(1,2)+Qsnew(1,ncolumns); %Wrap-around boundary condition for sediment
        %         Qsnew(1,ncolumns-1) = Qsnew(1, ncolumns-1)+Qsnew(1,1);
        
       
        %         if r>1, z(r,:) = z(r,:)-Qlost+Qslnew; end
        %         z(r+1,:) = z(r+1,:)+Qsinew;
        netsed(r, :) = netsed(r,:) - Qlost + Qslnew; 
        % ^ Take sediment out of current row that was transferred away and add sediment from lateral transfer
        netsed(r+1, :) = netsed(r+1,:) + Qsinew; %Transfer sediment to the next row
        %         figure(2), clf, plot(netsed(r,:)), hold on, plot(netsed(r+1,:),'r') %debugging
        %         pause %debugging
        slopeav(r,1)=slopesum(r,1)/sum(qcount(r,:)); %average slope of wet cells in row
        
    end %End of row loop
    %     netsed(nrows, :) = 0; %No bed elevation change of last row. Amount of sediment transport in = amount out.
    %     netsed(1, :) = 0; %No bed elevation change of first row.
    netsed(1,:) = netsed(1,:)+Qsinew; %Puts sediment leaving the bottom of the model domain onto the top row.
    netsed(nrows,:) = netsed(1,:); 
    % Netsed in the first row also includes the effects of lateral erosion, 
    %   whereas the loop stops before doing this for the last row. To 
    %   ensure the topography changes in the same way for the first and  
    %   last row, this line is needed.
    
    z = z+netsed; %Change bed elevation as a function of amount of sediment transferred
    %     if max(max(z))>6.5e7+10, pause, end
    
    % Determine vegetation distribution:
    T_resurrect = (1-plants).*T_resurrect+1;
    T_growth = plants.*T_growth+1;
    twoday_dep(:,:,1:99) = twoday_dep(:,:,2:100);
    twoday_dep(:,:,100) = netsed;
    dead_plants = union(find(sum(twoday_dep, 3) >=24/5*1e4), find(sum(twoday_dep,3) <= -24/5*1e4));
    plants(dead_plants) = 0;
    T_resurrect(dead_plants) = 0;
    T_growth(dead_plants) = 0;
    new_plants = find(T_resurrect)>=10;
    plants(new_plants) = 1;
    
    
    %-----------------------------------------------------------
    
    if mod(n, modarg) == 0
        Q_movie(:,:,n/modarg) = Q;
    end
    
    % wrap the last row back around to the first row
    Q(1,:) = Q(nrows,:);
    
end %End of iteration loop

% Save Q_movie, data for making movie (in a separate script called file_to_movie.m)
save(sprintf('../test_outputs/BraidFig_veg_.mat'), 'Q_movie')
