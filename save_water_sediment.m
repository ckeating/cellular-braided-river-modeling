function [Qnew, Qsinew] = save_water_sediment(Qi, Qsi, Qnew, Qsinew, c, ncolumns)
    % left edge case
    if c == 1 
        Qnew(1:2) = Qnew(1:2)+Qi(2:3);
        Qnew(ncolumns) = Qnew(ncolumns)+Qi(1);
        Qsinew(1:2) = Qsinew(1:2) + Qsi(2:3);
        Qsinew(ncolumns) = Qsinew(ncolumns)+Qsi(1);
    % right edge case
    elseif c == ncolumns 
        Qnew(ncolumns-1:ncolumns) = Qnew(ncolumns-1:ncolumns)+Qi(1:2);
        Qnew(1) = Qnew(1)+Qi(3);
        Qsinew(ncolumns-1:ncolumns) = Qsinew(ncolumns-1:ncolumns)+Qsi(1:2);
        Qsinew(1) = Qsinew(1)+Qsi(3);
    % general case
    else
        % Add transfers to Qnew matrix.
        % Qnew(c-1:c+1) = Qnew(c-1:c+1) + Qi; 
        % Faster:
        Qnew(c-1) = Qnew(c-1) + Qi(1); 
        Qnew(c) = Qnew(c) + Qi(2); 
        Qnew(c+1) = Qnew(c+1) + Qi(3);
        % Add transfers to Qsnew matrix
        % Qsinew(c-1:c+1) = Qsinew(c-1:c+1) + Qsi; 
        % Faster:
        Qsinew(c-1) = Qsinew(c-1) + Qsi(1);
        Qsinew(c) = Qsinew(c) + Qsi(2); 
        Qsinew(c+1) = Qsinew(c+1) + Qsi(3);
    end