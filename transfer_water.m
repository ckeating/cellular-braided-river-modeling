% Allocates discharge (water) from the current cell to one or more of its 
% three downstream cells.  
% Returns 3x1 vector of discharges in the three downstream cells.
function Qi = transfer_water(slps, Qi, r, c, N, Q)
    % If there is at least 1 positive slope
%     if max(slps)>0 
    if slps(1)>0 || (slps(2)>0 || slps(3)>0)
        slps2 = max(0, slps); % Reset negative slopes to 0. 
        % NEEDED: Don't reset this variable, since actual slope 
        %   determines sediment transfers below!!

        % Thomas paper, Eq 1.  Qi = discharge received by each
        %   of the downstream cells from the upstream cell
        Qi = Q(r,c).*(slps2.^N)/sum(slps2.^N);

    % If there are only negative slopes
    elseif max(slps)<0 
        % Take the absolute value of the slopes
        slps2 = abs(slps); 
        %   01/22/18 Colin: presumably the below rule (1B) is
        %   from the Thomas paper
        Qi = Q(r,c)./(slps2.^N.*sum(1./slps2.^N)); % RULE 1B.   

    % There is a combination of negatives and zeros
    else 
        myzeros = find(slps >= 0);
        mynegs = find(slps < 0);
        Qi(myzeros) = Q(r,c)./length(myzeros);
        Qi(mynegs) = 0;
    end