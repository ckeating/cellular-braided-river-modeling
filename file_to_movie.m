% Function for making movie out of discharge, topography, and plants/plant
% growth datasets
function file_to_movie(data, filename, custom_colormap, varargin)
    
    if length(varargin) > 0
        cmap_lower_limit = varargin{1};
        cmap_upper_limit = varargin{2};
    end
    
    fig=figure('Visible', 'off');
    % manual plot dimensions: 1120 width by 840 height
    set(gca(), 'XLimMode', 'manual', 'YLimMode', 'manual');
    set(gcf, 'Visible', 'off');
    
    fig.Visible = 'off';
    set(fig,'DoubleBuffer','on');
    
    % initialize videowriter object
    v = VideoWriter(char(filename), 'MPEG-4');
    v.FrameRate = 1;  % Default 30
    open(v);

    for frame_number=1:size(data,3)
        frame_number
        h = data(:,:,frame_number);
        set(gcf, 'Visible', 'off');
        pcolor(h);
        colormap(custom_colormap);
        colorbar
        % check for manual arguments for colormap scaling
        if length(varargin) > 0
            caxis([cmap_lower_limit, cmap_upper_limit]);
        end
        
        shading flat;
        axis equal;
        axis image;
        frame=getframe(gcf);
        writeVideo(v,frame);
    end

    close all;
end