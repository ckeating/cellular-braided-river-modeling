% Function for plotting scaled discharge data next to simulated discharge    
function file_to_movie_discharge(model_data, filename, fig_no, custom_colormap, ...
    niterations, nframes, extended_discharge_data)
    
    % initialize videowriter object
    v = VideoWriter(char(filename), 'MPEG-4');
    open(v);
    % sample the discharge data based on number of frames required
    sampled_extended_discharge = extended_discharge_data((niterations/nframes):(niterations/nframes):end);
    figure(fig_no);
    subplot(1,2,2)
    plot(1:length(sampled_extended_discharge),sampled_extended_discharge);
    title('Scaled Discharge');
    xlabel('Time Step');
    hold on
    for frame_number=1:size(model_data,3)
        frame_number
        h = model_data(:,:,frame_number);
        
        % plot data from model
        subplot(1,2,1)
        pcolor(h);
        colormap(custom_colormap);
        title('Scaled Discharge');
        shading flat;
        axis equal;
        axis image;
        hold on
        % plot points in black along the discharge graph
        subplot(1,2,2)
        scatter(frame_number,sampled_extended_discharge(frame_number), ...
            'filled', ...
            'MarkerFaceColor', [0 0 0], ...
            'MarkerEdgeColor', [0 0 0]);
        
        % get frame and write video
        frame=getframe(gcf);
        writeVideo(v,frame);
        
        % remove most recent feature (the point that was just plotted)
        children = get(gca, 'children');
        delete(children(1));
    end 


    close all;
end