function [Qsl, Qlost, Qslnew] = transfer_lateral_sediment(Klmin_coeff, Kl0, ...
    T_growth, Qlost, ncolumns, r, c, latslp, T_char, Qslnew)

    % Klmin is inversely related to bank-stability; lower Klmin
    %   should lead toward single-channel dynamics
    Klmin = Klmin_coeff*Kl0;

    % Updated 2/1/18 to correct the calculation of Kl
    Kl = double(Kl0+(Klmin-Kl0)) * (double(min(T_growth(r,c), T_char))/T_char);

    % Lateral sediment transfer received from the two neighboring cells
    % M&P 1997 p.1006
    Qsl = Kl*latslp*Qlost(c)./(1-Kl*latslp); 
    
    if c == 1
        Qlost([ncolumns 2]) = Qlost([ncolumns 2]) + Qsl;
        Qslnew([ncolumns 2]) = Qslnew([ncolumns 2]) + Qsl;
    elseif c == ncolumns
        Qlost([ncolumns-1 1]) = Qlost([ncolumns-1 1]) + Qsl;
        Qslnew([ncolumns-1 1]) = Qslnew([ncolumns-1 1]) + Qsl;
    else
        % Add sediment lost to lateral transfer to the total amount of 
        % sediment lost from the cell
        % Qlost([c-1 c+1]) = Qlost([c-1 c+1]) + Qsl; 
        % 44x faster than line above:
        Qlost(c-1) = Qlost(c-1) + Qsl(1); 
        Qlost(c+1) = Qlost(c+1) + Qsl(2);
        % Update Qslnew with lateral sediment transfer from current cell
        % Qslnew([c-1 c+1]) = Qslnew([c-1 c+1]) + Qsl;
        % much faster
        Qslnew(c-1) = Qslnew(c-1) + Qsl(1);
        Qslnew(c+1) = Qslnew(c+1) + Qsl(2);
    end