%% Script for running the model multiple times with different arguments

%% MODEL
% braided_veg_jf_updated

%% SYNTAX:
% function braided_veg_jf_updated (nrows, ncolumns, niterations, filename, ...
% include_veg, varargin)

% varargin includes:  nframes, Cs, write_videos, Klmin_coeff, ...
% plant_resurrect_time, initial_vegetation_growth, num_cells_discharge ...
% variable_discharge, discharge_timeseries_filename, total_discharge, ...
% save_final_state, load_final_state_filename,
% include_fixed_topo_condition

% uncomment line below if parpool is not on
% parpool(2)

tic

parfor k=int32(1 : 2)
    if k==1
        % with topo boundary condition
%         clear
        close all
        graphics_data1 = braided_veg_jf_updated(300, 50, 50, "../test_outputs/031218_w_topo_boundary", true, ...
            10, 3E5, true, 0.06, 10, 0, 10, false, '', 100000, false, '', true);
        parsave_simple('../test_outputs/graphics_data1', graphics_data1);
    end
    if k==2
        % without topo boundary condition
%         clear
        close all
        graphics_data2 = braided_veg_jf_updated(300, 50, 50, "../test_outputs/031218_wo_topo_boundary", true, ...
            10, 3E5, true, 0.06, 10, 0, 10, false, '', 100000, false, '', false);
        parsave_simple('../test_outputs/graphics_data2', graphics_data2);
    end
end  
graphics1_saved = load('../test_outputs/graphics_data1.mat');
create_model_graphics(graphics1_saved.C);
graphics2_saved = load('../test_outputs/graphics_data2.mat');
create_model_graphics(graphics2_saved.C);

toc
